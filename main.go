package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

var (
	fromEmail string
	toEmail   string
	sgAPI     string
	scrubDays string
)

func poolStatus() string {
	status := exec.Command("/usr/sbin/zpool", "status")

	out, err := status.CombinedOutput()

	if err != nil {
		log.Fatalf("run encountered an error: %s\n", err)
	}
	// Get the type
	// fmt.Printf("%T\n", out)
	// Print the output as a string
	// fmt.Print(string(out))

	return string(out)
}

func faultCheck(status string) string {
	var triggers = []string{"DEGRADED", "FAULTED", "OFFLINE", "UNAVAIL",
		"REMOVED", "FAIL", "DESTROYED", "corrupt",
		"cannot", "unrecover", "unavail"}

	var fault string

	for i := 0; i < len(triggers); i++ {
		if strings.Contains(status, triggers[i]) {
			fault = "fault"
		} else {
			fault = "healthy"
		}
	}

	return fault
}

func driveStatus() string {
	var drive string

	cmd := "/usr/sbin/zpool status | grep ONLINE | grep -v state | awk '{print $3 $4 $5}' | grep -v 000"
	out, _ := exec.Command("/usr/bin/bash", "-c", cmd).Output()

	if len(out) == 0 {
		drive = "no drive issues"
	} else {
		drive = "issues found on drive"
	}

	return drive
}

func poolCapacity() string {
	var status string
	cmd := "/usr/sbin/zpool list -H -o capacity"
	out, err := exec.Command("/usr/bin/bash", "-c", cmd).Output()

	if err != nil {
		log.Fatalf("run encountered an error: %s\n", err)
	}

	capacity := strings.TrimSpace(string(out))

	number, _ := strconv.Atoi(strings.Trim(capacity, "%"))

	if number < 80 {
		status = "capacity normal"
	} else {
		status = "capacity warning"
	}

	return status
}

func scrubCheck() string {
	var scrub string

	cmd := "/usr/sbin/zpool status | grep scrub | awk '{print $12 \" \"$13\" \"$14\" \" $15\" \" $16\" \"$17}'"
	out, err := exec.Command("/usr/bin/bash", "-c", cmd).Output()

	if err != nil {
		log.Fatalf("run encountered an error: %s\n", err)
	}

	dateFormat := "Jan 02 03:04:05 2006"

	parseDate, err := time.Parse(dateFormat, strings.TrimSpace(string(out)))

	if err != nil {
		fmt.Printf("error")
	}

	currentTime := time.Now()
	scrubInt, _ := strconv.Atoi(scrubDays)

	delta := currentTime.Sub(parseDate)

	if delta.Hours()/24 > float64(scrubInt) {
		scrub = "pool scrub needed"
	} else {
		scrub = "no scrub needed"
	}

	return scrub
}

func sendgridEmail(emailStatus, status string) {
	hostname, _ := os.Hostname()
	from := mail.NewEmail("zfs gobot", fromEmail)
	to := mail.NewEmail(os.Getenv("TO_EMAIL"), toEmail)
	subject := hostname + " - zpool status - " + emailStatus
	plainTextContent := status
	htmlContent := ""
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(sgAPI)

	response, err := client.Send(message)

	if err != nil {
		log.Fatalf("run encountered an error: %s\n", err)
	} else {
		fmt.Println(response.StatusCode)
	}
}

func main() {

	// Flags
	errorEmail := flag.Bool("error-only", false, "only email on errors")
	envFlag := flag.String("env", "", "env file to use")
	fromFlag := flag.String("from", "", "email to send from")
	toFlag := flag.String("to", "", "email to send to")
	sgFlag := flag.String("sgapi", "", "sendgrid api key")
	scrubFlag := flag.String("scrub", "8", "number of days inbetween scrubs")
	flag.Parse()

	// use an env file
	if *envFlag != "" {
		err := godotenv.Load(*envFlag)
		if err != nil {
			log.Fatalf("error loading env file")
		}
		fromEmail = os.Getenv("FROM_EMAIL")
		toEmail = os.Getenv("TO_EMAIL")
		sgAPI = os.Getenv("SENDGRID_API_KEY")
		// check for empty variables
		if fromEmail == "" || toEmail == "" || sgAPI == "" {
			log.Fatalf("missing environment variables")
		}
		// use the flags
	} else {
		fromEmail = *fromFlag
		toEmail = *toFlag
		sgAPI = *sgFlag
		// check for missing flags or empty values
		if fromEmail == "" || toEmail == "" || sgAPI == "" {
			log.Fatalf("missing flags")
		}
	}

	// scrubFlag is set seperately from everything above because it can modify either env or flags
	if os.Getenv("SCRUB_DAYS") == "" {
		scrubDays = *scrubFlag
	} else {
		scrubDays = os.Getenv("SCRUB_DAYS")
	}

	// get all the things
	status := poolStatus()
	fault := faultCheck(status)
	drives := driveStatus()
	capacity := poolCapacity()
	scrub := scrubCheck()

	// decide what the subject should be and send email
	if fault == "fault" {
		sendgridEmail(fault, status)
	} else if drives == "issues found on drive" {
		sendgridEmail(drives, status)
	} else if capacity == "capacity warning" {
		sendgridEmail(capacity, status)
	} else if scrub == "pool scrub needed" {
		sendgridEmail(scrub, status)
	} else {
		fmt.Printf("everything is good\n")
		if !*errorEmail {
			sendgridEmail(fault, status)
		}
	}

}
